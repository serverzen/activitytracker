===============
ActivityTracker
===============

A tool for querying various sources to see what time was worked on specific
tasks.  Contains various plugins for Tomboy notes and text files.

Usage
=====

The basic workflow with ActivityTracker is to define a "group" of
files that contain time-based task intervals in some format.  At
the moment, ActivityTracker comes with three different parsers:

  1. Emacs OrgMode_ files
  2. Tomboy notes
  3. Plain text files

Setting Up
----------

  1. Create a new directory, ``/home/myhomedir/.activitytracker``
  2. Setup a new file, ``/home/myhomedir/.activitytracker/config.ini``
  3. Add group entries to ``config.ini``

OrgMode_
--------

An entry for a group of OrgMode_ files looks like this::

  [File Group: Emacs OrgMode Files]
  base_dir = /somedir/org-files
  filename_match = [-_a-zA-Z0-9]*[.]org$
  parser_name = activitytracker.plugins.orgmode.EmacsOrgModeParser

Please see the OrgMode_ home page for specifics on the OrgMode_
format.  ActivityTracker's OrgMode parser has two requirements:

  1. Headings meant to be recorded/queried should have the **book** tag
  2. The second line after a heading with **book** tag should have a
     timestamp indicating the length of time worked

Here is an example entry:

  ** ActivityTracker :book:
  <2011-04-16 Sat 10:00-14:00>
  Working on orgmode support

Tomboy
------

An entry for a group of Tomboy_ notes on a recent Gnome configuration
would look like this:

  [File Group: Tomboy Notes]
  base_dir = /home/myhomedir/.local/share/tomboy
  filename_match = [a-zA-Z-0-9]*?[.]note
  parser_name = activitytracker.plugins.tomboy.TomboyFileParser

Plain Text Files
----------------

An entry for a group of plain text files would look like this:

  [File Group: Legacy Journal Files]
  base_dir = /home/myhomedir/Documents/journal
  filename_match = month-[a-zA-Z]+-[0-9]+[.]txt$
  parser_name = activitytracker.plugins.text.TextFileParser


.. _OrgMode: http://orgmode.org/
.. _Tomboy: http://projects.gnome.org/tomboy/?pagewanted=all

Writing Plugins
===============

The ActivityTracker plugin mechanism expects a *callable* to
be named as the ``parser_name`` value in a file entry group.

While checking files, the *callable* will be invoked (with no
arguments).  The *callable* must return an object with a
``parse_input`` function.  The ``parse_input`` function
will be invoked for every file matching the criteria.

After ``parse_input`` is invoked at least once, the object must
ensure it has a ``projects`` attribute as an iterable of
*Project* instances.

URLs
====

  * http://pypi.python.org/pypi/ActivityTracker
  * http://src.serverzen.com/activitytracker


Credits
=======

  * Created and maintained by Rocky Burt <rocky AT serverzen DOT com>.
